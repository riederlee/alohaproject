var express = require("express"),
    path = require('path'),
    bodyparser = require("body-parser");
    

        
var route = express.Router();
// Gallery Page
route.get('/gallery',(req,res)=> {
    fs.readFile('./public/Componont/gallery.html', null, function (error, data) {
        if (error) {
            res.writeHead(404);
            res.write('Whoops! File not found!');
        } else {
            res.write(data);
        }
        res.end();
    });
})

//Community Page
route.get('/community',(req,res)=> {
    fs.readFile('./public/Componont/community.html', null, function (error, data) {
        if (error) {
            res.writeHead(404);
            res.write('Whoops! File not found!');
        } else {
            res.write(data);
        }
        res.end();
    });
})

module.exports = route;